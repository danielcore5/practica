﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Modelo.Dal;
using Modelo.Entidades;
using Microsoft.Reporting.WebForms;
using System.Collections.Generic;
using System.IO;

namespace Taller_Entregable.Controllers
{
    public class EmpleadoController : Controller
    {
        private Inicio db = new Inicio();
        // GET: Empleado
        public ActionResult Index()
        {
            return View(db.registroempleado.ToList());
        }

        public ActionResult Crear()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(Empleado emple)
        {
            bool busqueda = db.registroempleado.Any(x => x.Cedula == emple.Cedula); ;
            if (busqueda == false)
            {
                if (ModelState.IsValid)
                {
                    db.registroempleado.Add(emple);
                    db.SaveChanges();
                }
            }
            else
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            emple = null;
            return RedirectToAction("Index");
        }

        public ActionResult Detalle(int id)
        {
            Empleado empleados = db.registroempleado.Find(id);
            if (empleados == null)
            {
                return HttpNotFound();
            }
            return View(empleados);
        }
        public ActionResult Editar(int id)
        {
            Empleado empleados = db.registroempleado.Find(id);
            return View(empleados);
        }

        [HttpPost]
        public ActionResult Editar(Empleado emple)
        {
            if (ModelState.IsValid)
            {
                db.Entry(emple).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(emple);
        }

        public ActionResult Eliminar(int id)
        {
            Empleado empleado = db.registroempleado.Find(id);
            return View(empleado);
        }

        [HttpPost, ActionName("Eliminar")]
        [ValidateAntiForgeryToken]
        public ActionResult confirmarEliminar(int id)
        {
            Empleado empleado = db.registroempleado.Find(id);
            db.registroempleado.Remove(empleado);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Report(string id)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reportes"), "ReporteEmpleados.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            List<Empleado> cm = new List<Empleado>();
            using (Inicio db = new Inicio())
            {
                cm = db.registroempleado.ToList();
            }
            ReportDataSource rd = new ReportDataSource("DataSetEmpleado", cm);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.5in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.5in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}