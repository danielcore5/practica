﻿using System.Linq;
using System.Web.Mvc;
using Modelo.Dal;
using Modelo.Entidades;
using System.Data.Entity;
using Microsoft.Reporting.WebForms;
using System.Collections.Generic;
using System.IO;

namespace Practica.Controllers
{
    public class HerramientaController : Controller
    {
        private Inicio db = new Inicio();
        // GET: Herramienta
        public ActionResult Index()
        {
            return View(db.registroherramienta.ToList());
        }

        public ActionResult Crear()
        {
            List<SelectListItem> listaEstadoHerramientas = new List<SelectListItem>();
            listaEstadoHerramientas.Add(new SelectListItem
            {
                Text = "Activo",
                Value = "True"
            });
            listaEstadoHerramientas.Add(new SelectListItem
            {
                Text = "Inactivo",
                Value = "False"
            });

            List<SelectListItem> listaDisponibilidadHerramientas = new List<SelectListItem>();
            listaDisponibilidadHerramientas.Add(new SelectListItem
            {
                Text = "Si",
                Value = "True"
            });
            listaDisponibilidadHerramientas.Add(new SelectListItem
            {
                Text = "No",
                Value = "False"
            });

            ViewBag.listaEstadoHerramientas = listaEstadoHerramientas;
            ViewBag.listaDisponibilidadHerramientas = listaDisponibilidadHerramientas;
            return View();
        }

        [HttpPost]
        public ActionResult Crear(Herramienta herram)
        {
            if (herram.Estado == false) {
                herram.Disponible = false;
            }
            bool busqueda = db.registroherramienta.Any(x => x.Serial == herram.Serial); ;
            if (busqueda == false)
            {
                if (ModelState.IsValid)
                {
                    db.registroherramienta.Add(herram);
                    db.SaveChanges();
                }
            }
            else
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            herram = null;
            return RedirectToAction("Index");
        }

        public ActionResult Detalle(int id)
        {
            Herramienta herramienta = db.registroherramienta.Find(id);
            if (herramienta == null)
            {
                return HttpNotFound();
            }
            return View(herramienta);
        }

        public ActionResult Editar(int id)
        {
            List<SelectListItem> listaEstadoHerramientas = new List<SelectListItem>();
            listaEstadoHerramientas.Add(new SelectListItem
            {
                Text = "Activo",
                Value = "true"
            });
            listaEstadoHerramientas.Add(new SelectListItem
            {
                Text = "Inactivo",
                Value = "false"
            });

            List<SelectListItem> listaDisponibilidadHerramientas = new List<SelectListItem>();
            listaDisponibilidadHerramientas.Add(new SelectListItem
            {
                Text = "Si",
                Value = "true"
            });
            listaDisponibilidadHerramientas.Add(new SelectListItem
            {
                Text = "No",
                Value = "false"
            });

            ViewBag.listaEstadoHerramientas = listaEstadoHerramientas;
            ViewBag.listaDisponibilidadHerramientas = listaDisponibilidadHerramientas;
            Herramienta herramienta = db.registroherramienta.Find(id);
            return View(herramienta);
        }

        [HttpPost]
        public ActionResult Editar(Herramienta herram)
        {
            if (ModelState.IsValid)
            {
                db.Entry(herram).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(herram);
        }

        public ActionResult Eliminar(int id)
        {
            Herramienta herramienta = db.registroherramienta.Find(id);
            return View(herramienta);
        }

        [HttpPost, ActionName("Eliminar")]
        [ValidateAntiForgeryToken]
        public ActionResult confirmarEliminar(int id)
        {
            Herramienta herramienta = db.registroherramienta.Find(id);
            db.registroherramienta.Remove(herramienta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Report(string id)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reportes"), "ReporteHerramientas.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            List<Herramienta> cm = new List<Herramienta>();
            using (Inicio db = new Inicio())
            {
                cm = db.registroherramienta.ToList();
            }
            ReportDataSource rd = new ReportDataSource("DataSetHerramienta", cm);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.5in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.5in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}