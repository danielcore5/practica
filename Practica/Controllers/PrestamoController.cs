﻿using System.Linq;
using System.Web.Mvc;
using Modelo.Dal;
using Modelo.Entidades;
using System.Data.Entity;
using Microsoft.Reporting.WebForms;
using System.Collections.Generic;
using System.IO;
using System;
using System.Net;
using System.Net.Http;

namespace Practica.Controllers
{
    public class PrestamoController : Controller
    {
        private Inicio db = new Inicio();
        // GET: Prestamo
        public ActionResult Index()
        {
            return View(db.registroprestamo.ToList());
        }

        [HttpPost]
        public ActionResult Index(int? serialParaBuscar)
        {
            if (serialParaBuscar != null)
            {
                List<Prestamo> listaPrestamos = new List<Prestamo>();
                listaPrestamos = db.registroprestamo
                    .SqlQuery("SELECT * FROM Prestamo WHERE Serial_Herramienta =" + serialParaBuscar)
                    .ToList();
                return View(listaPrestamos);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Crear()
        {
            List<Empleado> listaEmpleados = new List<Empleado>();
            listaEmpleados = db.registroempleado.ToList();
            listaEmpleados.ForEach(empleado => empleado.Nombres = empleado.Cedula + " - " + empleado.Nombres);
            ViewBag.dropdownEmpleados = listaEmpleados;

            List<Herramienta> listaHerramientas = new List<Herramienta>();
            listaHerramientas = db.registroherramienta
                .SqlQuery("SELECT * FROM Herramienta WHERE Estado = 'true' AND Disponible = 'true'")
                .ToList();
            listaHerramientas.ForEach(herramienta => herramienta.Descripcion = herramienta.Serial + " - " + herramienta.Descripcion);
            ViewBag.dropdownHerramientas = listaHerramientas;
            ViewBag.FechaPrestamo = DateTime.Now.ToString("yyyy/M/dd");
            return View();
        }

        [HttpPost]
        public ActionResult Crear(Prestamo prestamo)
        {
            Empleado empleado = db.registroempleado.Find(prestamo.Cedula_Empleado);
            prestamo.Nombres_Empleado = empleado.Nombres;
            Herramienta herramienta = db.registroherramienta.Find(prestamo.Serial_Herramienta);
            prestamo.Herramienta = herramienta.Descripcion;

            bool busqueda = db.registroprestamo.Any(x => x.IdPrestamo == prestamo.IdPrestamo); ;
            if (busqueda == false)
            {
                if (ModelState.IsValid)
                {
                    db.registroprestamo.Add(prestamo);
                    db.SaveChanges();
                    if (prestamo.Fecha_Prestamo != null && prestamo.Fecha_Devolucion == null)
                    {
                        String sqlStringQuery = "UPDATE Herramienta SET Disponible = 'false' WHERE Serial =" + prestamo.Serial_Herramienta;
                        db.Database.ExecuteSqlCommand(sqlStringQuery);
                    }
                }
                else
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
                }
            }
            else
            {
                var message = string.Join(" | ", ModelState.Values
         .SelectMany(v => v.Errors)
         .Select(e => e.ErrorMessage));
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, message);
            }
            prestamo = null;
            return RedirectToAction("Index");
        }

        public ActionResult Detalle(int id)
        {
            Prestamo prestamo = db.registroprestamo.Find(id);
            if (prestamo == null)
            {
                return HttpNotFound();
            }
            return View(prestamo);
        }

        public ActionResult Eliminar(int id)
        {
            Prestamo prestamo = db.registroprestamo.Find(id);
            if (prestamo.Fecha_Devolucion == null)
            {
                return View(prestamo);
            }
            TempData["msg"] = "<script>alert('No se puede eliminar un prestamo que ya tiene fecha de entrega');</script>";
            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Eliminar")]
        [ValidateAntiForgeryToken]
        public ActionResult confirmarEliminar(int id)
        {
            Prestamo prestamo = db.registroprestamo.Find(id);
            db.registroprestamo.Remove(prestamo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Editar(int id)
        {
            Prestamo prestamo = db.registroprestamo.Find(id);
            return View(prestamo);
        }

        [HttpPost, ActionName("Editar")]
        [ValidateAntiForgeryToken]
        public ActionResult confirmarEditar(String observaciones)
        {
            int idPrestamo = Int16.Parse(Request.FilePath.Substring(17));
            Prestamo prestamo = db.registroprestamo.Find(idPrestamo);
            prestamo.Observaciones = observaciones;
            if (ModelState.IsValid)
            {
                db.Entry(prestamo).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Devolucion(int id)
        {
            Prestamo prestamo = db.registroprestamo.Find(id);
            prestamo.Fecha_Devolucion = DateTime.Now.ToString("yyyy/M/dd");
            if (ModelState.IsValid)
            {
                db.Entry(prestamo).State = EntityState.Modified;
                db.SaveChanges();
                String sqlStringQuery = "UPDATE Herramienta SET Disponible = 'true' WHERE Serial =" + prestamo.Serial_Herramienta;
                db.Database.ExecuteSqlCommand(sqlStringQuery);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Report(string id)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Reportes"), "ReportePrestamos.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            List<Prestamo> cm = new List<Prestamo>();
            using (Inicio db = new Inicio())
            {
                cm = db.registroprestamo.ToList();
            }
            ReportDataSource rd = new ReportDataSource("DataSetPrestamos", cm);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.5in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.5in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);
            return File(renderedBytes, mimeType);
        }
    }
}