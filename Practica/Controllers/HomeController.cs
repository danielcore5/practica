﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Modelo.Dal;
using Modelo.Entidades;

namespace Taller_Entregable.Controllers
{
    public class HomeController : Controller
    {
        private Inicio db = new Inicio();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "Usuario,Clave")] SystemUser systemuser)
        {
            var user = systemuser.Usuario;
            var pass = systemuser.Clave;
            if (ModelState.IsValid)
            {
                var logo = db.systemuser.Where(
                        i => i.Usuario == user
                        &&
                        i.Clave == pass
                    ).ToList();

                if (logo.Count == 0)
                {
                    Session["Login"] = "Usuario o contraseña Incorrecta, Intente nuevamente";
                    Session["LoginUser"] = null;
                    return RedirectToAction("Index");
                }
                else
                {
                    Session["Login"] = null;
                    Session["LoginUser"] = systemuser.Usuario;
                    return RedirectToAction("Index", "Empleado");
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult Logout()
        {
            if (@Session["LoginUser"] != null)
            {
                @Session["LoginUser"] = null;
            }
            return RedirectToAction("Index");
        }
    }
}