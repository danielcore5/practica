USE master ;  
GO  

CREATE DATABASE Practica;
GO

USE [Practica]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Empleado](
	[Cedula] [int] NOT NULL,
	[Nombres] [nvarchar](50) NOT NULL,
	[Telefono] [int] NOT NULL,
 CONSTRAINT [PK_Empleado] PRIMARY KEY CLUSTERED 
(
	[Cedula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Herramienta](
	[Serial] [int] NOT NULL,
	[Descripcion] [varchar](20) NOT NULL,
	[Observacion] [varchar](100) NULL,
	[Estado] [bit] NOT NULL,
	[Disponible] [bit] NOT NULL,
 CONSTRAINT [PK_Herramienta] PRIMARY KEY CLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Prestamo](
	[IdPrestamo] [int] IDENTITY(1,1) NOT NULL,
	[Serial_Herramienta] [int] NOT NULL,
	[Herramienta] [varchar](50) NOT NULL,
	[Fecha_Prestamo] [varchar](20) NOT NULL,
	[Fecha_Devolucion] [varchar](20) NULL,
	[Observaciones] [varchar](50) NULL,
	[Cedula_Empleado] [int] NOT NULL,
	[Nombres_Empleado] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Prestamo] PRIMARY KEY CLUSTERED 
(
	[IdPrestamo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemUser](
	[Usuario] [nvarchar](15) NOT NULL,
	[Clave] [nvarchar](15) NOT NULL
) ON [PRIMARY]

INSERT INTO [dbo].[SystemUser] (Usuario, Clave)
VALUES ('admin2', 'admin2');

GO
ALTER TABLE [dbo].[Prestamo]  WITH CHECK ADD  CONSTRAINT [FK_Prestamo_Empleado] FOREIGN KEY([Cedula_Empleado])
REFERENCES [dbo].[Empleado] ([Cedula])
GO
ALTER TABLE [dbo].[Prestamo] CHECK CONSTRAINT [FK_Prestamo_Empleado]
GO
ALTER TABLE [dbo].[Prestamo]  WITH CHECK ADD  CONSTRAINT [FK_Prestamo_Herramienta] FOREIGN KEY([Serial_Herramienta])
REFERENCES [dbo].[Herramienta] ([Serial])
GO
ALTER TABLE [dbo].[Prestamo] CHECK CONSTRAINT [FK_Prestamo_Herramienta]
GO
