﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Modelo.Entidades
{
    public class Herramienta
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Serial { get; set; }

        [Required]
        public string Descripcion { get; set; }

        public string Observacion { get; set; }

        [Required]
        [UIHint("Estado")]
        public bool Estado { get; set; }

        [Required]
        [UIHint("Disponibilidad")]
        public bool Disponible { get; set; }

    }
}