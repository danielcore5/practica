﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Modelo.Entidades
{
    public class Prestamo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdPrestamo { get; set; }

        [Required]
        [Display(Name = "Cedula del empleado")]
        public int Cedula_Empleado { get; set; }

        [Display(Name = "Nombres del empleado")]
        public String Nombres_Empleado { get; set; }
        

        [Required]
        [Display(Name = "Serial de la herramienta")]
        public int Serial_Herramienta { get; set; }

        public String Herramienta { get; set; }

        [Required]
        [Display(Name = "Fecha del prestamo")]
        public String Fecha_Prestamo { get; set; }

        [Display(Name = "Fecha de la devolucion")]
        public String Fecha_Devolucion { get; set; }

        public String Observaciones { get; set; }
    }
}
