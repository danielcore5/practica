﻿using Modelo.Entidades;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Modelo.Dal
{
    public class Inicio : DbContext
    {
        public Inicio() : base("DBConexion") { }
        public DbSet<Empleado> registroempleado { get; set; }
        public DbSet<SystemUser> systemuser { get; set; }
        public DbSet<Herramienta> registroherramienta { get; set; }
        public DbSet<Prestamo> registroprestamo { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}